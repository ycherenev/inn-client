import { mount } from "@vue/test-utils";
import Vue from "vue";
import Index from "@/pages/index.vue";

describe("Inn", () => {
  const wrapper = mount(Index);

  it("renders the input", () => {
    expect(wrapper.html()).toContain(
      'input placeholder="Inn" type="text" name="inn">'
    );
  });

  it("has a button", () => {
    expect(wrapper.contains("button")).toBe(true);
  });

  it("poulates inn list", async () => {
    wrapper.setData({
      inn_list: [
        {
          id: 1,
          inserted_at: "2012-02-01",
          check_result: false,
          value: 1234123434
        }
      ]
    });
    await Vue.nextTick();
    const innItem = wrapper.find(".collection-item> span");

    expect(innItem.text().replace(/ {2}|\r\n|\n|\r/gm, "")).toContain(
      "1234123434 [2012-02-01] :incorrect"
    );
  });
});
